DAEMON_VERSION=$(shell cat VERSION)

CC=g++
PKGCONFIG=`pkg-config --cflags --libs liblog4cxx dbus-c++-1 gtkmm-3.0`
CFLAGS=-pedantic -Wall -W -g $(PKGCONFIG) -DDOUANE_VERSION=\"$(DAEMON_VERSION)\" -std=c++11
LDFLAGS=$(PKGCONFIG) -lpthread

OBJ=src/dbus/dbus_client.o \
	src/dbus/douane.o \
	src/gtk/gtk_application_icon_sublimer.o \
	src/gtk/gtk_box_unknown_application.o \
	src/gtk/gtk_process_icon.o \
	src/gtk/gtk_question_window.o \
	src/main.o \
	src/network_activity.o \
	src/thread.o \
	src/tools.o

USR_BIN_PATH=/usr/bin
INSTALL=$(USR_BIN_PATH)/install -c
BINDIR=$(DESTDIR)/opt/douane/bin
DATADIR=$(DESTDIR)/opt/douane/data
PIDSDIR=$(DESTDIR)/opt/douane/pids
EXEC=douane-dialog

all: $(EXEC)

$(EXEC): $(OBJ)
	$(CC) -o $(EXEC) $(OBJ) $(LDFLAGS)

%.o: %.cpp
	$(CC) $(CFLAGS) -o $@ -c $<

clean:
	rm -rf $(OBJ) $(EXEC)

install: $(EXEC)
	test -d $(BINDIR) || mkdir -p $(BINDIR)
	test -d $(DATADIR) || mkdir -p $(DATADIR)
	install -m 0555 $(EXEC) $(BINDIR)
	install -m 0755 data/* $(DATADIR)
	test -f $(USR_BIN_PATH)/$(EXEC) && rm -f $(USR_BIN_PATH)/$(EXEC) || true
	ln -s $(BINDIR)/$(EXEC) $(USR_BIN_PATH)

uninstall: $(EXEC)
	# When using System D, stops the services, removes the files, and reload
	# systemd
	killall -q doaune-dialog || exit 0
	# Removes dialog files
	rm -f $(BINDIR)/$(EXEC)
